#include <gtest/gtest.h>

#include "fuzzy.h"

#define ARRAY_LENGTH(a) (sizeof(a) / sizeof(a[0]))

#define VALUES_TRIANGLE   -0.25, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25
#define VALUES_TRAP       -0.25, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3, 3.25

constexpr float values[] ={VALUES_TRIANGLE};
constexpr float valuesT[] = {VALUES_TRAP};

class fuzzyLogicTest : public ::testing::Test
{
public:
  fuzzyLogicTest()
  {
    _testFuzzy.set_member_triangle(&_triangleMember,0,1,2);
    _testFuzzy.set_member_trapezoid(&_trapezoidMember, 0,1,2,3);
  }

  fuzzy   _testFuzzy;
  triangle _triangleMember;
  trapezoid _trapezoidMember;
};

TEST_F(fuzzyLogicTest, testSetMember){
    // testing the good fill of structures.
    // triangle member
    EXPECT_EQ(_triangleMember.low_, 0);
    EXPECT_EQ(_triangleMember.medium_, 1);
    EXPECT_EQ(_triangleMember.high_, 2);
    // trapezoid member
    EXPECT_EQ(_trapezoidMember.low_, 0);
    EXPECT_EQ(_trapezoidMember.medium_1, 1);
    EXPECT_EQ(_trapezoidMember.medium_2, 2);
    EXPECT_EQ(_trapezoidMember.high_, 3);
}

TEST_F(fuzzyLogicTest, test_get_member_triangle){
  float ret = 0;
  float _inT = 0;
  for(int i = 0;  i < ARRAY_LENGTH(values); i++){
    ret =  _testFuzzy.get_member_triangle(&_triangleMember,values[i]);
    if(values[i] <= _triangleMember.low_){
      EXPECT_EQ(ret, 0);
    }
    else if(values[i] < _triangleMember.medium_){
      _inT = (values[i]  - _triangleMember.low_) / (_triangleMember.medium_ - _triangleMember.low_);
      EXPECT_EQ(ret,_inT);
      _inT = 0;
    }
    else if(values[i] == _triangleMember.medium_){
      EXPECT_EQ(ret, 1);
    }
    else if(values[i] < _triangleMember.high_){
      _inT = (_triangleMember.high_ - values[i]) / (_triangleMember.high_ - _triangleMember.medium_);
      EXPECT_EQ(ret, _inT);
      _inT = 0;
    }
    else{
      EXPECT_EQ(ret,0);
    }
  }
}

TEST_F(fuzzyLogicTest, test_get_member_trapezoid){
  float ret = 0;
  float _inT = 0;
  for(int i = 0; i < ARRAY_LENGTH(valuesT); i++){
    ret = _testFuzzy.get_member_trapezoid(&_trapezoidMember,valuesT[i]);
    if(valuesT[i] <= _trapezoidMember.low_){
      EXPECT_EQ(ret, 0);
    }
    else if(valuesT[i] < _trapezoidMember.medium_1){
      _inT = (valuesT[i] - _trapezoidMember.low_) / (_trapezoidMember.medium_1 - _trapezoidMember.low_);
      EXPECT_EQ(ret,_inT);
      _inT = 0;
    }
    else if(valuesT[i] < _trapezoidMember.medium_2){
      EXPECT_EQ(ret, 1);
    }
    else if (valuesT[i] < _trapezoidMember.high_){
      _inT = (_trapezoidMember.high_ - valuesT[i]) / (_trapezoidMember.high_ - _trapezoidMember.medium_2);
      EXPECT_EQ(ret, _inT);
      _inT = 0;
    }
    else{
      EXPECT_EQ(ret,0);
    }
  }
}

TEST_F(fuzzyLogicTest, test_fuzzy_OR){
float val1 = 0;
float val2 = 3;


}
