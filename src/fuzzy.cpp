#include "fuzzy.h"

fuzzy::fuzzy(void){

}

/*** fuzzification methods ***/
void fuzzy::set_member_triangle(triangle *_triangle, float _min, float _medium, float _max){
  _triangle->low_ = _min;
  _triangle->medium_ = _medium;
  _triangle->high_ = _max;
}

void fuzzy::set_member_trapezoid(trapezoid *_trapezoid, float _min, float _medium_1, float _medium_2, float _max){
  _trapezoid->low_ = _min;
  _trapezoid->medium_1 = _medium_1;
  _trapezoid->medium_2 = _medium_2;
  _trapezoid->high_ = _max;
}

float fuzzy::get_member_triangle(triangle *_triangle, float t){
  float output;
  if (t <= _triangle->low_){
    output = 0;
  }
  else if (t < _triangle->medium_){
    output =  (t - _triangle->low_) / (_triangle->medium_ - _triangle->low_);
  }
  else if (t == _triangle->medium_){
    output =  1;
  }
  else if (t < _triangle->high_){
    output =  (_triangle->high_ - t) / (_triangle->high_ - _triangle->medium_);
  }
  else{
    output =  0;
  }
  return output;
}

float fuzzy::get_member_trapezoid(trapezoid *_trapezoid, float t){
  float output;
  if (t <= _trapezoid->low_){
    output =  0;
  }
  else if (t < _trapezoid->medium_1){
    output =  (t - _trapezoid->low_) / (_trapezoid->medium_1 - _trapezoid->low_);
  }
  else if (t < _trapezoid->medium_2){
    output =  1;
  }
  else if (t < _trapezoid->high_){
    output =  (_trapezoid->high_ - t) / (_trapezoid->high_ - _trapezoid->medium_2);
  }
  else {
    output =  0;
  }
  return output;
}

/*** Set of inference engine rules ***/

float fuzzy::fuzzy_OR(float _value1, float _value2){
  return std::max(_value1, _value2);
}

float fuzzy::fuzzy_AND(float _value1, float _value2){
  return std::min(_value1, _value2);
}

float fuzzy::fuzzy_NOT(float _value){
  return (1 - _value);
}

/*** defuzzification methods ***/


void fuzzy::set_output_member_trapezoid(output *_output,
                                        float _min,
                                        float _middle_left,
                                        float _middle_right,
                                        float _max){
  _output->min_ = _min;
  _output->middle_1 = _middle_left;
  _output->middle_2 = _middle_right;
  _output->max_ = _max;
}

float fuzzy::get_trapezoid_area(output *_trapezoid){
  float med_1 = (_trapezoid->middle_1 - _trapezoid->min_) * _trapezoid->strength;
  float med_2 = (_trapezoid->max_ - _trapezoid->middle_2) * _trapezoid->strength;
  float top = med_2 - med_1;
  float base = _trapezoid->max_ - _trapezoid->min_;
  return (_trapezoid->strength * (base + top)) / 2;
}



float fuzzy::get_output_COG_two(output *_output_1,
                                output *_output_2) {
  float centerOne = ((_output_1->max_ - _output_1->min_) / 2) + _output_1->min_;
  float centerTwo = ((_output_2->max_ - _output_2->min_) / 2) + _output_2->min_;
  float area_1 = get_trapezoid_area(_output_1);
  float area_2 = get_trapezoid_area(_output_2);
  return (centerOne * _output_1->strength + centerTwo * _output_2->strength) / (area_1 + area_2);
}

float fuzzy::get_output_COG_three(output *_output_1,
                                  output *_output_2,
                                  output *_output_3){
  float centerOne = ((_output_1->max_ - _output_1->min_) / 2) + _output_1->min_;
  float centerTwo = ((_output_2->max_ - _output_2->min_) / 2) + _output_2->min_;
  float centerThree = ((_output_3->max_ - _output_3->min_) / 2) + _output_3->min_;
  float area_1 = get_trapezoid_area(_output_1);
  float area_2 = get_trapezoid_area(_output_2);
  float area_3 = get_trapezoid_area(_output_3);
  return (centerOne * _output_1->strength + centerTwo * _output_2->strength + centerThree * _output_3->strength) / (area_1 + area_2 + area_3);
}

float fuzzy::get_output_COG_four(output *_output_1,
                                  output *_output_2,
                                  output *_output_3,
                                  output *_output_4){
  float centerOne = ((_output_1->max_ - _output_1->min_) / 2) + _output_1->min_;
  float centerTwo = ((_output_2->max_ - _output_2->min_) / 2) + _output_2->min_;
  float centerThree = ((_output_3->max_ - _output_3->min_) / 2) + _output_3->min_;
  float centerFour = ((_output_4->max_ - _output_4->min_) / 2) + _output_4->min_;
  float area_1 = get_trapezoid_area(_output_1);
  float area_2 = get_trapezoid_area(_output_2);
  float area_3 = get_trapezoid_area(_output_3);
  float area_4 = get_trapezoid_area(_output_4);
  return (centerOne * _output_1->strength + centerTwo * _output_2->strength + centerThree * _output_3->strength + centerFour * _output_4->strength) / (area_1 + area_2 + area_3 + area_4);
}
