#ifndef FUZZY_H
#define FUZZY_H

#include "stdio.h"
#include <iostream>
#include <algorithm>    // std::max and std::min


/*********** STRUCTURES DEFINITION ****************/

/*** membership function fuzzification ***/
typedef struct{
  float low_;
  float medium_;
  float high_;
} triangle;

typedef struct{
  float low_;
  float medium_1; // left middle
  float medium_2; // right middle
  float high_;
} trapezoid;

/*** membership function defuzzification output ***/

typedef struct{
  float min_;
  float middle_1; // middle left
  float middle_2; // middle right
  float max_;
  float strength;
} output;

/*********** ClASS DEFINITION ********************/

class fuzzy{
public:

  fuzzy(void);

  /*** fuzzification step ***/
  // Setting of each fuzzified member value
  void set_member_triangle(triangle *_triangle, float _min, float _medium, float _max);
  void set_member_trapezoid(trapezoid *_trapezoid, float _min, float _medium_1, float _medium_2, float _max);

  // Getting the value of each fuzzified member value
  float get_member_triangle(triangle *_triangle, float t);
  float get_member_trapezoid(trapezoid *_trapezoid, float t);

  /*** Set of rules for inference engine ***/

  float fuzzy_OR(float _value1, float _value2);
  float fuzzy_AND(float _value1, float _value2);
  float fuzzy_NOT(float _value);

  /*** defuzzification step ***/

  void set_output_member_trapezoid(output *_output,
                                  float _min,
                                  float _middle_left,
                                  float _middle_right,
                                  float _max);

float get_output_COG_two(output *_output_1,
                        output *_output_2);

float get_output_COG_three(output *_output_1,
                          output *_output_2,
                          output *_output_3);

float get_output_COG_four(output *_output_1,
                          output *_output_2,
                          output *_output_3,
                          output *_output_4);


private:

  float get_trapezoid_area(output *_trapezoid);

};

#endif /* FUZZY_H */
